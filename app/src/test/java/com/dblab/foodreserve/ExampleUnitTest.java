package com.dblab.foodreserve;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class ExampleUnitTest {
    @Test
    public void addition_isCorrect() {
        String text = "2018-06-09T07:30:45.543+04:30";
        assertEquals("2018-06-09", text.substring(0,10));
    }
}