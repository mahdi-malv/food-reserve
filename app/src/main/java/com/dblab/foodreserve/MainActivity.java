package com.dblab.foodreserve;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.dblab.foodreserve.food_personal.PReportActivity;
import com.dblab.foodreserve.food_personal.PUserMgmtActivity;
import com.dblab.foodreserve.model.D;
import com.dblab.foodreserve.model.Utils;
import com.dblab.foodreserve.model.user.UserStore;
import com.dblab.foodreserve.student.SCreditActivity;

import java.util.ArrayList;
import java.util.List;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

import static com.dblab.foodreserve.LoginActivity.isStudent;

public class MainActivity extends AppCompatActivity {

    RecyclerView list;
    static long doubleBackToExitPressedOnce = 0, timePassedToolbar = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setSupportActionBar((Toolbar) findViewById(R.id.toolbar));
    }

    @Override
    protected void onStart() {
        super.onStart();
        init();
        initList();
        //region Toolbar easter egg
        (findViewById(R.id.toolbar)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (System.currentTimeMillis() - timePassedToolbar <= 1000) {
                    new AlertDialog.Builder(MainActivity.this)
                            .setTitle("Developers")
                            .setMessage("Ali Javadi\nHasan Khalooei")
                            .setPositiveButton("Ok", null)
                            .create().show();
                }
                timePassedToolbar = System.currentTimeMillis();
            }
        });
        //endregion
    }

    private void init() {
        list = findViewById(R.id.list);
        String toolbarText = isStudent ?
                "پورتال دانشجو" : "پورتال تغذیه";
        ((TextView) findViewById(R.id.toolbar).findViewById(R.id.toolbarText))
                .setText(toolbarText);
    }

    private void initList() {
        list.setLayoutManager(new GridLayoutManager(this, 2));
        List<String> set = new ArrayList<>();
        List<Class> classset = new ArrayList<>();
        if (isStudent) {
            set.add("اطلاعیه ها");//NotificationActivity
            set.add("برنامه ی هفتگی");//WeeklyScheduleActivity
            set.add("مدیریت اعتبار");//SCreditActivity
            classset.add(NotificationActivity.class);
            classset.add(WeeklyScheduleActivity.class);
            classset.add(SCreditActivity.class);
        } else {
            set.add("اطلاعیه ها");//NotificationActivity
            set.add("برنامه ی هفتگی");//WeeklyScheduleActivity
            set.add("افزودن کاربر");//PUserMgmtActivity
            set.add("مشاهده گزارش");//PReportActivity
            classset.add(NotificationActivity.class);
            classset.add(WeeklyScheduleActivity.class);
            classset.add(PUserMgmtActivity.class);
            classset.add(PReportActivity.class);
        }

        list.setAdapter(new MyAdapter(this, set, classset));
    }

    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce + 2000 > System.currentTimeMillis()) {
            super.onBackPressed();
            finishAffinity();
        } else {
            Toast
                    .makeText(getApplicationContext(),
                            "برای خروج دوباره بازگشت را بزنید",
                            Toast.LENGTH_SHORT)
                    .show();
            doubleBackToExitPressedOnce = System.currentTimeMillis();
        }
    }

    //region Menu

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        menu.add(0, 0, 0, "خروج از حساب");
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case 0:
                String text = "آیا میخواهید از حساب خارج شوید؟\nشناسه: ";
                UserStore s = new UserStore(this);
                if (s.getUserType() == D.USER_TYPE_ADMIN)
                    text += s.getAdmin().getId();
                else
                    text += s.getStudent().getStudentNumber();

                new AlertDialog.Builder(this)
                        .setTitle("خروج")
                        .setMessage(text)
                        .setPositiveButton("بله", (d, w) -> {
                            logout();
                        })
                        .setNegativeButton("انصراف", null)
                        .create().show();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void logout() {
        finish();
        new UserStore(this).logout();
        startActivity(new Intent(this, LoginActivity.class));
    }

    //endregion

    private class MyAdapter extends RecyclerView.Adapter<MainActivity.ViewHolder> {

        Context c;
        List<String> dataset;
        List<Class> classset;

        MyAdapter(Context c, List<String> dataset, List<Class> classset) {
            this.c = c;
            this.dataset = dataset;
            this.classset = classset;
        }

        @Override
        public MainActivity.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            return new MainActivity.ViewHolder(
                    LayoutInflater
                            .from(parent.getContext())
                            .inflate(R.layout.pmain_item, parent, false)
            );

        }

        @Override
        public void onBindViewHolder(final MainActivity.ViewHolder holder, int position) {
            holder.text.setText(dataset.get(position));
            int color = new Utils().getRandomColor();
            holder.text.setBackgroundColor(color);
            if (new Utils().isColorDark(color)) {
                //Dark
                holder.text.setTextColor(Color.parseColor("#FFFFFF"));
            } else {
                holder.text.setTextColor(Color.parseColor("#000000"));
            }
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startActivity(new Intent(c, classset.get(holder.getAdapterPosition())));
                }
            });
        }

        @Override
        public int getItemCount() {
            return dataset.size();
        }
    }

    private class ViewHolder extends RecyclerView.ViewHolder {

        TextView text;

        public ViewHolder(View itemView) {
            super(itemView);
            text = itemView.findViewById(R.id.text);
        }
    }

    //region Font
    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
    //endregion
}
