package com.dblab.foodreserve;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.dblab.foodreserve.model.D;
import com.dblab.foodreserve.model.db.DB;
import com.dblab.foodreserve.model.db.Feed;
import com.dblab.foodreserve.model.db.Student;
import com.dblab.foodreserve.model.user.UserStore;

import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class LoginActivity extends AppCompatActivity implements
        RadioGroup.OnCheckedChangeListener,
        View.OnClickListener {

    RadioGroup loginGroup;
    RadioButton foodPersonal, student;
    EditText userNameEdit, passEdit;
    FloatingActionButton okFab;
    TextView toolbarText;
    ImageView imageView;
    public static boolean isStudent = false;

    //region Font
    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    //endregion

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        setSupportActionBar(findViewById(R.id.toolbar));
        init();
        imageView.setVisibility(View.INVISIBLE);
        animateIcon(imageView);
    }

    @Override
    protected void onStart() {
        super.onStart();
        LoginActivity.isStudent = true;
        UserStore store = new UserStore(this);
        if (store.getUserLogged()) {
            Intent i = new Intent(this, MainActivity.class);
            if (store.getUserType() == D.USER_TYPE_STUDENT) {
                isStudent = true;
                startActivity(i);
                finish();
            } else if (store.getUserType() == D.USER_TYPE_ADMIN) {
                isStudent = false;
                startActivity(i);
                finish();
            }
        }
        student.setChecked(true);
        foodPersonal.setChecked(false);
    }

    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {
        if (checkedId == student.getId()) {
            //Student checked
            toolbarText.setText("ورود دانشجو");
            userNameEdit.setHint("شماره دانشجوئی");
            passEdit.setHint("کد ملی");
        } else {
            //Food personal checked
            toolbarText.setText("ورود مسئول تغذیه");
            userNameEdit.setHint("شناسه پرسنلی");
            passEdit.setHint("رمز ورود");
        }
    }

    private void init() {
        loginGroup = findViewById(R.id.loginType);
        loginGroup.setOnCheckedChangeListener(this);
        student = findViewById(R.id.student);
        foodPersonal = findViewById(R.id.foodPersonal);
        userNameEdit = findViewById(R.id.userNameEdit);
        passEdit = findViewById(R.id.passEdit);
        okFab = findViewById(R.id.okFab);
        imageView = findViewById(R.id.imageView);
        toolbarText = ((Toolbar) findViewById(R.id.toolbar)).findViewById(R.id.toolbarText);
        okFab.setOnClickListener(this);
    }

    @SuppressWarnings("ConstantConditions")
    @Override
    public void onClick(View v) {
        //Fab click
        //TODO: validate fields
        //TODO: onCorrect: Connect to DB
        boolean isValidated;
        if (isStudent) {
            isValidated = userNameEdit.getText().toString().length() == 10;
        } else {
            isValidated = userNameEdit.getText().toString().length() > 3 && userNameEdit.getText().toString().length() <= 10;
        }
        isValidated &= passEdit.getText().toString().length() > 3 && passEdit.getText().toString().length() <= 25;
        LoginActivity.isStudent = student.isChecked() && !foodPersonal.isChecked();

        if (LoginActivity.isStudent && isValidated) {
            DB.getInstance(this).studentDao().getByNumber(userNameEdit.getText().toString())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.computation())
                    .subscribe(new SingleObserver<Student>() {
                        @Override
                        public void onSubscribe(Disposable d) {
                        }

                        @Override
                        public void onSuccess(Student student) {
                            if (student.getPass().equals(passEdit.getText().toString())) {
                                new UserStore(LoginActivity.this).saveUser(student);
                                startActivity(new Intent(LoginActivity.this, MainActivity.class));
                                finish();
                            } else {
                                Toast.makeText(LoginActivity.this, "رمز اشتباه", Toast.LENGTH_SHORT).show();
                            }
                        }

                        @Override
                        public void onError(Throwable e) {
                            Toast.makeText(LoginActivity.this, "دانشجو در دیتابیس ناموجود", Toast.LENGTH_SHORT).show();
                        }
                    });
        } else if (!isStudent && isValidated) {
            DB.getInstance(this).feedDao()
                    .getById(Integer.parseInt(userNameEdit.getText().toString()))
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.computation())
                    .subscribe(new SingleObserver<Feed>() {
                        @Override
                        public void onSubscribe(Disposable d) {
                        }

                        @Override
                        public void onSuccess(Feed feed) {
                            if (feed.getPass().equals(passEdit.getText().toString())) {
                                new UserStore(LoginActivity.this).saveUser(feed);
                                startActivity(new Intent(LoginActivity.this, MainActivity.class));
                                finish();
                            } else {
                                Toast.makeText(LoginActivity.this, "رمز اشتباه", Toast.LENGTH_SHORT).show();
                            }
                        }

                        @Override
                        public void onError(Throwable e) {
                            Toast.makeText(LoginActivity.this, "شناسه ناموجود", Toast.LENGTH_SHORT).show();
                        }
                    });
        }
    }

    private void animateIcon(View v) {
        Animation animation = AnimationUtils.loadAnimation(this, R.anim.fade_icon);
        animation.setDuration(2000);
        v.startAnimation(animation);
        v.setVisibility(View.VISIBLE);
    }
}
