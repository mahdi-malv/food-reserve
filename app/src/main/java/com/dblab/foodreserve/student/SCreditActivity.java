package com.dblab.foodreserve.student;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.v4.widget.TextViewCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.InputType;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.dblab.foodreserve.R;
import com.dblab.foodreserve.model.db.DB;
import com.dblab.foodreserve.model.user.UserStore;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class SCreditActivity extends AppCompatActivity {

    TextView credit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scredit);
        credit = findViewById(R.id.text);
        setTitle("مدیریت اعتبار");
        TextViewCompat
                .setAutoSizeTextTypeWithDefaults(
                        credit,
                        TextViewCompat.AUTO_SIZE_TEXT_TYPE_UNIFORM);
        getCredit();
    }

    @SuppressLint("CheckResult")
    private void getCredit() {
        DB.getInstance(this)
                .studentDao()
                .getMoney(new UserStore(this).getStudent().getStudentNumber())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.computation())
                .subscribe(credit -> {
                    SCreditActivity.this.credit.setText(credit + " تومان");
                }, throwable -> {
                    SCreditActivity.this.credit.setText("دوباره امتحان کنید");
                });
    }

    public void onClickAddMoneyFab(View view) {
        EditText editText = new EditText(this);
        editText.setInputType(InputType.TYPE_CLASS_NUMBER);
        new AlertDialog.Builder(this)
                .setTitle("افزایش اعتبار")
                .setMessage("اعتبار مورد نظر را وارد کنید.")
                .setView(editText)
                .setPositiveButton("تایید", (d, w) -> {
                    int credit = Integer.parseInt(editText.getText().toString());
                    new Thread(() -> {
                        DB.getInstance(SCreditActivity.this)
                                .studentDao()
                                .addCredit(new UserStore(SCreditActivity.this).getStudent().getStudentNumber(), credit);
                    }).start();
                    recreate();
                })
                .setNegativeButton("انصراف", null)
                .create()
                .show();
    }

    //region Font
    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
    //endregion
}
