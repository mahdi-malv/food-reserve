package com.dblab.foodreserve.food_personal;

import android.content.Context;
import android.database.sqlite.SQLiteConstraintException;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatButton;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.dblab.foodreserve.R;
import com.dblab.foodreserve.model.db.DB;
import com.dblab.foodreserve.model.db.Student;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class PUserMgmtActivity extends AppCompatActivity {

    EditText user, pass;
    AppCompatButton button;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_puser_mgmt);
        setTitle("افزودن دانشجو");
        init();

        button.setOnClickListener(v -> {
            final boolean[] isOk = {false};
            new Thread(() -> {
                try {
                    DB.getInstance(PUserMgmtActivity.this)
                            .studentDao()
                            .insert(new Student(
                                    user.getText().toString(),
                                    pass.getText().toString()
                            ));
                    isOk[0] = true;
                } catch (SQLiteConstraintException e) {
                    Log.e("Error", e.getMessage());
                    isOk[0] = false;
                }
            }
            ).start();

            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            } finally {
                if (isOk[0]) {
                    Toast.makeText(this, "دانشجو اضافه شد", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(this, "دانشجو قبلا ثبت شده.", Toast.LENGTH_SHORT).show();
                }
            }
            finish();
        });
    }

    private void init() {
        user = findViewById(R.id.number);
        pass = findViewById(R.id.pass);
        button = findViewById(R.id.button);
    }

    //region Font
    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
    //endregion
}
