package com.dblab.foodreserve.food_personal;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import com.dblab.foodreserve.R;
import com.dblab.foodreserve.model.D;
import com.dblab.foodreserve.model.db.DB;

import org.joda.time.DateTime;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import noman.weekcalendar.WeekCalendar;
import noman.weekcalendar.listener.OnDateClickListener;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class PReportActivity extends AppCompatActivity implements OnDateClickListener {

    WeekCalendar calendar;
    TextView text;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_preport);
        init();
    }

    private void init() {
        calendar = findViewById(R.id.calendar);
        calendar.setOnDateClickListener(this);
        text = findViewById(R.id.text);
        text.setText("روز را انتخاب کنید.");
    }

    @SuppressLint("CheckResult")
    @Override
    public void onDateClick(DateTime dateTime) {
        String date = dateTime.toString().substring(0, 10);
        final String[] t = {"تاریخ: " + date + "\n"};
        DB.getInstance(this)
                .reserveDao()
                .getNumberOfReserves(date, D.MEAL_LAUNCH)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.computation())
                .subscribe(sum -> {
                    t[0] += "\n" + "تعداد رزرو ناهار: ";
                    t[0] += sum;
                    text.setText(t[0]);
                }, throwable -> {
                    t[0] += "\n" + "تعداد رزرو ناهار ";
                    t[0] += "نامشخص";
                    text.setText(t[0]);
                });

        DB.getInstance(this)
                .reserveDao()
                .getNumberOfReserves(date, D.MEAL_DINNER)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.computation())
                .subscribe(integer -> {
                    t[0] += "\n" + "تعداد رزرو شام: ";
                    t[0] += integer;
                    text.setText(t[0]);
                }, throwable -> {
                    t[0] += "\n" + "تعداد رزرو شام: ";
                    t[0] += "نامشخص";
                    text.setText(t[0]);
                });
        text.setText(t[0]);
    }

    //region Font
    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
    //endregion
}
