package com.dblab.foodreserve.food_personal;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatButton;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.dblab.foodreserve.R;
import com.dblab.foodreserve.model.db.DB;
import com.dblab.foodreserve.model.db.Notification;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class AddNotificationActivity extends AppCompatActivity implements View.OnClickListener {

    EditText titleEdit, contentEdit;
    AppCompatButton add;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_notification);
        init();
    }

    private void init() {
        titleEdit = findViewById(R.id.titleEdit);
        contentEdit = findViewById(R.id.contentEdit);
        add = findViewById(R.id.add);
        add.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                DB.getInstance(AddNotificationActivity.this)
                        .notifDao()
                        .insertNotif(new Notification(
                                titleEdit.getText().toString(),
                                contentEdit.getText().toString()
                        ));
            }
        }).start();
        Toast.makeText(this, "اضافه خواهد شد.", Toast.LENGTH_SHORT).show();
        finish();
    }

    //region Font
    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
    //endregion
}
