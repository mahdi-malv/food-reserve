package com.dblab.foodreserve;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.dblab.foodreserve.food_personal.AddNotificationActivity;
import com.dblab.foodreserve.model.PersianDate;
import com.dblab.foodreserve.model.db.DB;
import com.dblab.foodreserve.model.db.Notification;

import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Flowable;
import io.reactivex.FlowableSubscriber;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class NotificationActivity extends AppCompatActivity {

    RecyclerView list;
    FloatingActionButton addFab;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification);
        setSupportActionBar((Toolbar) findViewById(R.id.toolbar));
        init();

        addFab.setOnClickListener(v -> startActivity(new Intent(NotificationActivity.this, AddNotificationActivity.class)));
    }

    @Override
    protected void onStart() {
        super.onStart();
        initList();
        if (LoginActivity.isStudent) addFab.setVisibility(View.GONE);
        else addFab.setVisibility(View.VISIBLE);
    }

    private void init() {
        addFab = findViewById(R.id.addFab);
        list = findViewById(R.id.list);
    }

    @SuppressLint("CheckResult")
    private void initList() {
        list.setHasFixedSize(false);
        list.setLayoutManager(new LinearLayoutManager(this));
        List<Notification> set = new ArrayList<>();
        list.setAdapter(new MyAdapter(set));
        //TODO: Add disposable
        DB.getInstance(this)
                .notifDao()
                .getAll()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.computation())
                .flatMap(Flowable::fromIterable)
                .subscribe(item -> {
                    set.add(item);
                    list.getAdapter().notifyDataSetChanged();
                });
    }

    private class MyAdapter extends RecyclerView.Adapter<ViewHolder> {

        List<Notification> dataset;

        public MyAdapter(List<Notification> dataset) {
            this.dataset = dataset;
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            return new ViewHolder(
                    LayoutInflater
                            .from(parent.getContext())
                            .inflate(R.layout.notification_item, parent, false)
            );

        }

        @Override
        public void onBindViewHolder(ViewHolder holder, int position) {

            Notification item = dataset.get(position);
            holder.title.setText(item.getTitle());
            holder.content.setText(item.getText());
            holder.date.setText(new PersianDate().getShamsi(item.getDateStamp()));
        }

        @Override
        public int getItemCount() {
            return dataset.size();
        }
    }

    private class ViewHolder extends RecyclerView.ViewHolder {

        public TextView title, content, date;

        public ViewHolder(View itemView) {
            super(itemView);
            title = itemView.findViewById(R.id.title);
            content = itemView.findViewById(R.id.content);
            date = itemView.findViewById(R.id.date);
        }
    }

    //region Font
    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
    //endregion
}
