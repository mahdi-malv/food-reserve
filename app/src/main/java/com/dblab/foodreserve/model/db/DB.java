package com.dblab.foodreserve.model.db;

import android.arch.persistence.db.SupportSQLiteDatabase;
import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;
import android.support.annotation.NonNull;

@Database(entities = {Feed.class, Notification.class, Student.class, Schedule.class, Reserve.class}, version = 1, exportSchema = false)
public abstract class DB extends RoomDatabase {

    public abstract FeedDao feedDao();

    public abstract StudentDao studentDao();

    public abstract NotifDao notifDao();

    public abstract ScheduleDao scheduleDao();

    public abstract ReserveDao reserveDao();

    private static DB instance;

    public static DB getInstance(Context c) {
        if (instance == null) instance = buildDB(c);
        return instance;
    }

    /**
     * First time creation of database
     * @param c the page we are in
     * @return an Opened database
     */
    private static DB buildDB(final Context c) {
        return Room.databaseBuilder(c, DB.class, "food.db")
                .addCallback(new Callback() {
                    @Override
                    public void onCreate(@NonNull SupportSQLiteDatabase db) {
                        super.onCreate(db);
                        new Thread(() -> DB.getInstance(c).feedDao().addFeedGuy(new Feed(1, "1234"),
                                new Feed(2, "1234"),
                                new Feed(3, "1234"),
                                new Feed(94, "9412120102"))).start();
                    }
                }).build();
    }
}
