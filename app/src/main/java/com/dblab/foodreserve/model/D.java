package com.dblab.foodreserve.model;

/**
 * Holds Constant data
 */
public class D {

    public static final String NO_FOOD = "غذایی امروز ثبت نشده است.";

    public static final String[] foods = {
            "چلوخورش قرمه سبزی",
            "چلوخورش آلو",
            "چلوخورش قیمه",
            "جوجه کباب",
            "چلو کباب کوبیده",
            "چلوخورش فسنجان",
            "باقالی پلو با نوتلا",
            "ماکارانی با آب هویج",
            "قارچ پلو :|",
            "چلو کوبیده مرغ با مرگ موش",
            "پاستا با ماست موسیر",
            "آش دوغ با آب نبات چوبی"
    };

    public static final int PRICE = 1000;

    public static final String USER_TYPE = "user_type";
    public static final int USER_TYPE_ADMIN = 1;
    public static final int USER_TYPE_STUDENT = 0;
    public static final String USER_STORE = "user";
    public static final String NUMBER = "number";
    public static final String ID = "id";
    public static final String PASS = "pass";
    public static final String LOG_CHECK = "logged_in";

    //Reserve meals
    public static final int MEAL_LAUNCH = 0;
    public static final int MEAL_DINNER = 1;
}
