package com.dblab.foodreserve.model.db;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Index;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

@Entity(tableName = "schedule",
        indices = {@Index(value = {"date", "meal"}, unique = true)}
)
public class Schedule {

    @PrimaryKey(autoGenerate = true)
    int id;

    @NonNull
    private String date;

    private int code;

    private int meal;

    public Schedule(@NonNull String date, int code, int meal) {
        this.date = date;
        this.code = code;
        this.meal = meal;
    }

    @NonNull
    public String getDate() {
        return date;
    }

    public void setDate(@NonNull String date) {
        this.date = date;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public int getMeal() {
        return meal;
    }

    public void setMeal(int meal) {
        this.meal = meal;
    }
}
