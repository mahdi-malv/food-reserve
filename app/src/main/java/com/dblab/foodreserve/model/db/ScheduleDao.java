package com.dblab.foodreserve.model.db;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import java.util.List;

import io.reactivex.Flowable;
import io.reactivex.Single;

@Dao
public interface ScheduleDao {

    @Query("select * from schedule where date = :date")
    Flowable<List<Schedule>> getByDate(String date);

    @Delete
    void delete(Schedule schedule);

    @Query("delete from schedule where date = :date and meal = :meal")
    void delete(String date, int meal);

    @Insert
    void insert(Schedule schedule);
}
