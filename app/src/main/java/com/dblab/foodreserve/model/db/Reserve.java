package com.dblab.foodreserve.model.db;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.Index;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

/**
 * This is not exactly an entity
 * It's a relation which holds Number as StudentNumber
 */
@Entity(
        tableName = "reserve",
        indices = {@Index(value = {"number", "date", "meal"}, unique = true)},
        foreignKeys = {@ForeignKey(entity = Student.class, parentColumns = "student_number", childColumns = "number")}
)
public class Reserve {

    @PrimaryKey(autoGenerate = true)
    int id;

    @NonNull
    private
    String number;

    @NonNull
    private
    String date;

    private int meal;

    public Reserve(@NonNull String number, @NonNull String date, int meal) {
        this.number = number;
        this.date = date;
        this.meal = meal;
    }

    @NonNull
    public String getNumber() {
        return number;
    }

    public void setNumber(@NonNull String number) {
        this.number = number;
    }

    @NonNull
    public String getDate() {
        return date;
    }

    public void setDate(@NonNull String date) {
        this.date = date;
    }

    public int getMeal() {
        return meal;
    }

    public void setMeal(int meal) {
        this.meal = meal;
    }
}
