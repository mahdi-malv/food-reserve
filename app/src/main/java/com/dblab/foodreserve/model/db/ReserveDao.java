package com.dblab.foodreserve.model.db;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import java.util.List;

import io.reactivex.Flowable;
import io.reactivex.Single;

@Dao
public interface ReserveDao {


    @Query("select * from reserve where number = :number and date = :date")
    Flowable<List<Reserve>> getByStudentNumberAndDate(String number, String date);

    @Query("select count(*) as sum from reserve where date = :date and meal = :meal")
    Single<Integer> getNumberOfReserves(String date, int meal);

    @Delete
    void cancelMeal(Reserve reserve);

    @Query("delete from reserve where date = :date and meal = :meal")
    void cancelAllMeals(String date, int meal);

    @Query("update student set credit = credit + :price where student_number in (select number from reserve where date = :date and meal = :meal)")
    void returnCredit(String date, int meal, int price);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void reserveMeal(Reserve reserve);

}
