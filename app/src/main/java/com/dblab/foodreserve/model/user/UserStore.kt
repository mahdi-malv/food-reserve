package com.dblab.foodreserve.model.user

import android.content.Context
import android.content.SharedPreferences
import com.dblab.foodreserve.model.D
import com.dblab.foodreserve.model.D.*
import com.dblab.foodreserve.model.db.Feed
import com.dblab.foodreserve.model.db.Student


class UserStore(c: Context) {
    private val preferences: SharedPreferences = c.getSharedPreferences(USER_STORE, Context.MODE_PRIVATE)

    val userLogged: Boolean
        get() = preferences.getBoolean(LOG_CHECK, false)

    /**
     * @return -1 for Unspecified
     * @return 0 for Customer
     * @return 1 for admin
     * Notice that if
     */
    fun getUserType() = preferences.getInt(D.USER_TYPE, -1)

    fun saveUser(user: Student) {
        val editor = preferences.edit()
        editor.putInt(D.USER_TYPE, D.USER_TYPE_STUDENT)

        editor.putString(NUMBER, user.studentNumber)
        editor.putString(PASS, user.pass)
        setLogged(true)
        editor.apply()
    }

    fun saveUser(admin: Feed) {
        val editor = preferences.edit()
        editor.putInt(D.USER_TYPE, D.USER_TYPE_ADMIN)
        editor.putInt(ID, admin.id)
        editor.putString(PASS, admin.pass)

        setLogged(true)
        editor.apply()
    }

    fun getUser(): Any {
        return if (getUserType() == D.USER_TYPE_STUDENT)
            getStudent()
        else
            getAdmin()
    }

    fun getStudent(): Student {
        return Student(
                preferences.getString(NUMBER, "NoNumber"),
                preferences.getString(PASS, "NoPass")
        )
    }

    fun getAdmin(): Feed {
        return Feed(
                preferences.getInt(ID, -1),
                preferences.getString(PASS, "NoPass")
        )
    }

    private fun setLogged(logged: Boolean) {
        val editor = preferences.edit()
        editor.putBoolean(LOG_CHECK, logged)
        editor.apply()
    }

    fun logout() {
        preferences.edit().clear().apply()
        setLogged(false)
    }
}
