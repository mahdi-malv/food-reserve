package com.dblab.foodreserve.model.db;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import java.util.List;

import io.reactivex.Flowable;

@Dao
public interface NotifDao {

    @Insert
    void insertNotif(Notification notification);

    @Query("select * from notif")
    Flowable<List<Notification>> getAll();
}
