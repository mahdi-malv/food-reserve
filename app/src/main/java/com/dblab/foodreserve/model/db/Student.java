package com.dblab.foodreserve.model.db;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

@Entity(tableName = "student")
public class Student {

    @PrimaryKey
    @ColumnInfo(name = "student_number")
    @NonNull
    private String studentNumber;

    @ColumnInfo(name = "pass")
    @NonNull
    private String pass;

    private int credit;

    public Student(@NonNull String studentNumber, @NonNull String pass) {
        this.studentNumber = studentNumber;
        this.pass = pass;
        credit = 0;
    }

    @NonNull
    public String getStudentNumber() {
        return studentNumber;
    }

    public void setStudentNumber(@NonNull String studentNumber) {
        this.studentNumber = studentNumber;
    }

    @NonNull
    public String getPass() {
        return pass;
    }

    public void setPass(@NonNull String pass) {
        this.pass = pass;
    }

    public int getCredit() {
        return credit;
    }

    public void setCredit(int credit) {
        this.credit = credit;
    }
}
