package com.dblab.foodreserve.model.db;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import io.reactivex.Single;

@Dao
public interface StudentDao {

    @Query("select * from student where student_number = :number")
    Single<Student> getByNumber(String number);

    @Query("select credit from student where student_number = :number")
    Single<Integer> getMoney(String number);

    @Query("update student set credit = credit + :credit where student_number = :number")
    void addCredit(String number, int credit);

    @Query("update student set credit = credit - :credit where student_number = :number")
    void decreaseCredit(String number, int credit);


    @Insert(onConflict = OnConflictStrategy.FAIL)
    void insert(Student student);
}
