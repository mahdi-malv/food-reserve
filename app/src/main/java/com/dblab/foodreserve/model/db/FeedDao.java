package com.dblab.foodreserve.model.db;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import io.reactivex.Single;

@Dao
public interface FeedDao {

    @Query("select * from feed where id = :id")
    Single<Feed> getById(int id);

    @Insert
    void addFeedGuy(Feed... feeds);
}
