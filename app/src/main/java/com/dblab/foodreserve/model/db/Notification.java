package com.dblab.foodreserve.model.db;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

@Entity(tableName = "notif")
public class Notification {

    @PrimaryKey
    private long dateStamp;

    @NonNull
    private String title;

    @NonNull
    private String text;

    public Notification(@NonNull String title, @NonNull String text) {
        this.title = title;
        this.text = text;
        this.dateStamp = System.currentTimeMillis();
    }

    public long getDateStamp() {
        return dateStamp;
    }

    public void setDateStamp(long dateStamp) {
        this.dateStamp = dateStamp;
    }

    @NonNull
    public String getTitle() {
        return title;
    }

    public void setTitle(@NonNull String title) {
        this.title = title;
    }

    @NonNull
    public String getText() {
        return text;
    }

    public void setText(@NonNull String text) {
        this.text = text;
    }
}
