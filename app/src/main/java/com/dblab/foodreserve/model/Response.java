package com.dblab.foodreserve.model;

public interface Response {
    public void onComplete(boolean result);
}
