package com.dblab.foodreserve;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.AppCompatDelegate;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.dblab.foodreserve.model.D;
import com.dblab.foodreserve.model.Utils;
import com.dblab.foodreserve.model.db.DB;
import com.dblab.foodreserve.model.db.Reserve;
import com.dblab.foodreserve.model.db.Schedule;
import com.dblab.foodreserve.model.db.Student;
import com.dblab.foodreserve.model.user.UserStore;

import org.joda.time.DateTime;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import noman.weekcalendar.WeekCalendar;
import noman.weekcalendar.listener.OnDateClickListener;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class WeeklyScheduleActivity extends AppCompatActivity implements
        OnDateClickListener, View.OnClickListener {

    WeekCalendar calendar;
    TextView launch, dinner;
    String date = "";
    DateTime dateTime;
    boolean isLaunchReserved = false, isDinnerReserved = false;
    boolean isLaunchAdded = false, isDinnerAdded = false;
    UserStore store;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_weekly_schedule);
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
        init();
        store = new UserStore(this);
    }

    private void init() {
        calendar = findViewById(R.id.calendar);
        calendar.setOnDateClickListener(this);
        launch = findViewById(R.id.launch);
        launch.setOnClickListener(this);
        dinner = findViewById(R.id.dinner);
        dinner.setOnClickListener(this);
    }

    @Override
    public void onDateClick(DateTime dateTime) {
        launch.setText("در حال دریافت اطلاعات...");
        dinner.setText("در حال دریافت اطلاعات...");
        date = dateTime.toString().substring(0, 10);
        this.dateTime = dateTime;
        getMeals();
        getReservationStatusIfStudent();
    }

    @SuppressLint("CheckResult")
    private void getMeals() {
        //region Get meals and add here
        DB.getInstance(this)
                .scheduleDao()
                .getByDate(date)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.computation())
                .subscribe(schedules -> {
                    String launchText = D.NO_FOOD, dinnerText = D.NO_FOOD;
                    for (Schedule s : schedules) {
                        if (s.getMeal() == D.MEAL_LAUNCH) {
                            launchText = D.foods[s.getCode()];
                            isLaunchAdded = true;
                        } else if (s.getMeal() == D.MEAL_DINNER) {
                            dinnerText = D.foods[s.getCode()];
                            isDinnerAdded = true;
                        } else
                            Toast.makeText(WeeklyScheduleActivity.this, "This MUST be assertion Error. WTH?",
                                    Toast.LENGTH_SHORT).show();
                    }
                    launch.setText(launchText);
                    dinner.setText(dinnerText);
                }, throwable -> {
                    Log.e("Error#Week", throwable.getMessage());
                    launch.setText(D.NO_FOOD);
                    dinner.setText(D.NO_FOOD);
                });

        //endregion
    }

    @SuppressLint("CheckResult")
    private void getReservationStatusIfStudent() {
        if (LoginActivity.isStudent) {
            //region Get student reservation status
            DB.getInstance(this)
                    .reserveDao()
                    .getByStudentNumberAndDate(
                            store.getStudent().getStudentNumber(),
                            date)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.computation())
                    .subscribe(reserves -> {
                        isLaunchReserved = false;
                        isDinnerReserved = false;
                        for (Reserve r : reserves) {
                            if (r.getMeal() == D.MEAL_LAUNCH) isLaunchReserved = true;
                            else if (r.getMeal() == D.MEAL_DINNER) isDinnerReserved = true;
                        }
                        //Colorize if reserved
                        if (isLaunchReserved) launch
                                .setBackgroundColor(Color.parseColor("#94fccc"));
                        else launch
                                .setBackgroundColor(Color.parseColor("#f7c1c2"));
                        if (isDinnerReserved) dinner
                                .setBackgroundColor(Color.parseColor("#94fccc"));
                        else dinner
                                .setBackgroundColor(Color.parseColor("#f7c1c2"));
                    }, throwable -> {
                        isLaunchReserved = false;
                        isDinnerReserved = false;
                        launch.setBackgroundColor(Color.parseColor("#f7c1c2"));
                        dinner.setBackgroundColor(Color.parseColor("#f7c1c2"));
                    });
            //endregion
        }
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.launch) {
            if (date.isEmpty()) return;
            if (dateTime.toDate().getTime() < new Utils().getToday()) {
                //It's from before week
                Toast.makeText(this, "قادر به تغییر روزهای قبل نیستید.", Toast.LENGTH_SHORT).show();
            } else
                clicked(isLaunchReserved, isLaunchAdded, D.MEAL_LAUNCH);
        } else if (v.getId() == R.id.dinner) {
            if (date.isEmpty()) return;
            if (dateTime.toDate().getTime() < new Utils().getToday()) {
                //It's from before week
                Toast.makeText(this, "قادر به تغییر هفته قبل نیستید.", Toast.LENGTH_SHORT).show();
            } else
                clicked(isDinnerReserved, isDinnerAdded, D.MEAL_DINNER);
        }
    }

    void clicked(boolean mealReserved, boolean mealAdded, int meal) {
        if (LoginActivity.isStudent) {
            //if reserved -> Cancel option else Reserve
            if (!mealReserved && mealAdded) {
                //Reserve
                //TODO: Check credit first
                //todo: decrease it if ok
                alert("رزرو", "آبا قصد رزرو وعده را دارید؟",
                        (d, w) -> {
                            DB.getInstance(this)
                                    .studentDao()
                                    .getMoney(store.getStudent().getStudentNumber())
                                    .observeOn(AndroidSchedulers.mainThread())
                                    .subscribeOn(Schedulers.computation())
                                    .subscribe(integer -> {
                                        if (integer < D.PRICE) {
                                            Toast.makeText(this, "اعتبار به اندازه ی کافی ندارید.", Toast.LENGTH_SHORT).show();
                                        } else {
                                            new Thread(() -> {
                                                Student s = new UserStore(this).getStudent();
                                                DB.getInstance(WeeklyScheduleActivity.this)
                                                        .reserveDao()
                                                        .reserveMeal(new Reserve(
                                                                s.getStudentNumber(),
                                                                date,
                                                                meal
                                                        ));
                                                DB.getInstance(WeeklyScheduleActivity.this)
                                                        .studentDao()
                                                        .decreaseCredit(s.getStudentNumber(), D.PRICE);

                                            }).start();
                                        }
                                    }, throwable -> {
                                        Toast.makeText(this, "بررسی اعتبار ناموفق. دوباره امتحان کنید.", Toast.LENGTH_SHORT).show();
                                    });
                        });
//                recreate();
            } else if (mealReserved && mealAdded) {
                //Cancel
                alert("لغو", "آبا قصد لغو رزرو وعده را دارید؟",
                        (d, w) -> {
                            new Thread(() -> {
                                DB.getInstance(WeeklyScheduleActivity.this)
                                        .studentDao()
                                        .addCredit(store.getStudent().getStudentNumber(), D.PRICE);
                                DB.getInstance(WeeklyScheduleActivity.this)
                                        .reserveDao()
                                        .cancelMeal(new Reserve(
                                                (store.getStudent().getStudentNumber()),
                                                date,
                                                meal
                                        ));
                            }).start();
                        });
//                recreate();
            } else {
                Toast.makeText(this, "لغو یا رزرو ناممکن. وعده ای ثبت نشده است.", Toast.LENGTH_SHORT).show();
            }
        } else {
            //If added remove else add
            if (mealAdded) {
                //remove
                alert("حذف", "آبا قصد حذف وعده را دارید؟",
                        (d, w) -> {

                            new Thread(() -> {
                                //Return credit to reserved student
                                DB.getInstance(WeeklyScheduleActivity.this)
                                        .reserveDao()
                                        .returnCredit(date, meal, D.PRICE);
                                //Delete from schedule
                                DB.getInstance(WeeklyScheduleActivity.this)
                                        .scheduleDao()
                                        .delete(date, meal);
                                //Delete reserves
                                DB.getInstance(WeeklyScheduleActivity.this)
                                        .reserveDao()
                                        .cancelAllMeals(date, meal);
                            }).start();
                        });
//                recreate();
            } else {
                //add
                Spinner spinner = new Spinner(this);
                spinner.setAdapter(new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item,
                        D.foods));
                new AlertDialog.Builder(this)
                        .setTitle("افزودن")
                        .setMessage("غذا را انتخاب کنید.")
                        .setView(spinner)
                        .setPositiveButton("تایید", (d, w) -> {
                            int code = spinner.getSelectedItemPosition();
                            new Thread(() -> {
                                DB.getInstance(WeeklyScheduleActivity.this)
                                        .scheduleDao()
                                        .insert(new Schedule(date, code, meal));
                            }).start();
//                            recreate();
                        })
                        .setNegativeButton("انصراف", null)
                        .create().show();

            }
        }
    }

    private void alert(String title, String message, DialogInterface.OnClickListener listener) {
        new AlertDialog.Builder(this)
                .setTitle(title)
                .setMessage(message)
                .setPositiveButton("بله", listener)
                .setNegativeButton("انصراف", null)
                .create()
                .show();
    }

    //region Font
    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
    //endregion
}
